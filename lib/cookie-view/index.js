
/**
 * Module dependencies.
 */

var debug = require('visionmedia/debug')('cookie-view');
var domify = require('component/domify');
var remove = require('hughsk/remove-element');
var Events = require('component/events');
var Emitter = require('component/emitter');
var rndid = require('stephenmathieson/rndid');
var template = require('./view.jade');

/**
 * Expose `CookieView`.
 */

module.exports = CookieView;

/**
 * Create a view for `cookie`.
 *
 * @param {Object} cookie
 * @api public
 */

function CookieView(cookie){
  if (!(this instanceof CookieView)) {
    return new CookieView(cookie);
  }

  this.cookie = cookie;
  this.el = domify(template(cookie));
  this.id = this.el.id = rndid();
  this.events = new Events(this.el, this);
  this.bind();
}

/**
 * Mixin `Emitter`.
 */

Emitter(CookieView.prototype);

/**
 * Bind event listeners.
 *
 * @api private
 */

CookieView.prototype.bind = function(){
  this.events.bind('click .remove', 'onremove');
};

/**
 * Remove button click callback.
 *
 * @api private
 */

CookieView.prototype.onremove = function(){
  debug('remove: "%s"', this.cookie.name);
  this.emit('remove', this.cookie);
};

/**
 * Remove the view from the DOM.
 *
 * @return {CookieView}
 * @api public
 */

CookieView.prototype.remove = function(){
  debug('removing cookie view #%s view', this.id);
  remove(this.el);
};
