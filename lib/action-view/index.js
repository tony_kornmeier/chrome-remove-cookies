
/**
 * Module dependencies.
 */

var debug = require('visionmedia/debug')('action-view');
var domify = require('component/domify');
var Events = require('component/events');
var Emitter = require('component/emitter');
var empty = require('yields/empty');
var rndid = require('stephenmathieson/rndid');
var pipe = require('stephenmathieson/eventemitter-pipe');

var CookieView = require('../cookie-view');
var template = require('./view.jade');

/**
 * Expose `ActionView`.
 */

module.exports = ActionView;

/**
 * Create an action view.
 *
 * @api public
 */

function ActionView(){
  if (!(this instanceof ActionView)) {
    return new ActionView;
  }

  this.cookies = [];
  this.items = [];
  this.el = domify(template());
  this.ul = this.el.querySelector('ul.cookies');
  this.id = this.el.id = rndid();
  this.events = new Events(this.el, this);
  this.bind();
}

/**
 * Mixin `Emitter`.
 */

Emitter(ActionView.prototype);

/**
 * Bind events.
 *
 * @api private
 */

ActionView.prototype.bind = function(){
  debug('binding event listeners');
  this.events.bind('click button.remove-all', 'onremoveall');
};

/**
 * Remove all button callback.
 *
 * @api private
 */

ActionView.prototype.onremoveall = function(){
  this.emit('remove all');
};

/**
 * Add a `Cookie` from `obj`.
 *
 * @param {Object} obj
 * @return {ActionView}
 * @api public
 */

ActionView.prototype.addCookie = function(obj){
  var li = document.createElement('li');
  var view = new CookieView(obj);

  li.appendChild(view.el);
  this.ul.appendChild(li);

  // proxy CookieView events
  pipe(view, this);

  // save
  this.cookies.push(obj);
  this.items.push(li);

  return this;
};

/**
 * Remove cookie of `name`.
 *
 * @param {String} name
 * @throws {Error} if cookie `name` is not found
 * @return {ActionView}
 * @api public
 */

ActionView.prototype.removeCookie = function(name){
  if (!~(index = this.findCookie(name))) {
    throw new Error(
      fmt('no cookie "%s"', name)
    );
  }

  debug('removing cookie "%s" from DOM', name);
  // remove from DOM
  this.items[index].remove();

  // remove from lists
  debug('removing cookie "%s" from lists', name);
  this.items.splice(index, 1);
  this.cookies.splice(index, 1);

  debug('%d cookies left', this.cookies.length);

  // display empty message if necessary
  if (!this.cookies.length) {
    setTimeout(this.emptyMessage.bind(this));
  }

  return this;
};

/**
 * Display an "empty" message.
 *
 * @api private
 */

ActionView.prototype.emptyMessage = function(){
  debug('displaying "empty" message');

  var status = this.el.querySelector('.status');
  var msg = document.createElement('p');

  msg.className = 'empty';
  msg.innerText = 'No cookies';

  empty(status);

  console.log(status)
  console.log(msg)

  status.appendChild(msg);
};

/**
 * Find a cookie's index by its `name`.
 *
 * @param {String} name
 * @return {Number}
 * @api private
 */

ActionView.prototype.findCookie = function(name){
  // lookup
  var index = -1;
  for (var i = 0, cookie; cookie = this.cookies[i]; i++) {
    if (name == cookie.name) {
      index = i;
      break;
    }
  }

  debug('cookie "%s" is at index %d', name, index);
  return index;
};

/**
 * Remove all cookies.
 *
 * @return {ActionView}
 * @api public
 */

ActionView.prototype.reset = function(){
  // empty status messages
  empty(this.el.querySelector('.status'));

  this.items.forEach(function(li){
    li.remove();
  });
  this.items.length = 0;
  this.cookies.length = 0;
  return this;
};

/**
 * Display the given `err`.
 *
 * @param {Error} err
 * @return {ActionView}
 * @api public
 */

ActionView.prototype.error = function(err){
  var status = this.el.querySelector('.status');
  var error = document.createElement('p');
  error.className = 'error';
  error.innerText = err.message;

  // empty previous messages
  empty(status);

  // add Chrome error
  if (err.original) {
    var original = document.createElement('p');
    original.className = 'original';
    original.innerText = err.original.message;

    error.appendChild(original);
  }

  // display error
  status.appendChild(error);

  return this;
};

/**
 * Unbind events.
 *
 * @api private
 */

ActionView.prototype.unbind = function(){
  debug('removing event listeners');
  this.events.unbind();
};
