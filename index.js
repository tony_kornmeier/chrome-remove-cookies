
/**
 * Module dependencies.
 */

var debug = require('visionmedia/debug')('action');
var url = require('component/url');
var parallel = require('component/array-parallel');
var fmt = require('yields/fmt');
var View = require('./lib/action-view');

/**
 * DOM refs.
 */

var body = document.body;

/**
 * Insert view.
 */

var view = new View;
debug('inserting action view (#%s)', view.id);
body.appendChild(view.el);

/**
 * Listen for `remove` events.
 */

view.on('remove', function(cookie){
  removeCookie(cookie, function(err){
    if (err) view.error(err);
  });
});

/**
 * Listen for `remove all` events.
 */

view.on('remove all', function(){
  // build removal functions
  var removals = view.cookies.map(function(cookie){
    return function(done){
      removeCookie(cookie, done);
    };
  });

  // TODO: disable all buttons in view
  // TODO: add spinner

  // run all
  debug('remove %d cookies', removals.length);
  parallel(removals, function(err){
    if (err) view.error(err);
    view.reset();
  });
});

/**
 * Get cookies for the active tab.
 */

var opts = { active: true, lastFocusedWindow: true }
chrome.tabs.query(opts, function (tabs) {
  var str = tabs[0].url;
  var obj = url.parse(str);

  var opts = { domain: obj.hostname };
  chrome.cookies.getAll(opts, function(cookies){
    // add each cookie to the view
    cookies.forEach(function(cookie){
      view.addCookie(cookie);
    });
  });
});

/**
 * Remove the given `cookie`, invoking `fn(err)`.
 *
 * @param {Object} cookie
 * @param {Function} [fn]
 * @api private
 */

function removeCookie(cookie, fn){
  fn = fn || noop;
  var opts = {
      name: cookie.name
    , url: 'http' + (cookie.secure ? 's' : '') + '://'
           + cookie.domain + cookie.path
  };

  debug('remove: %j', opts);
  chrome.cookies.remove(opts, function(_){
    if (_) {
      view.removeCookie(cookie.name);
      return fn();
    }

    var err = new Error(
      fmt('unable to remove cookie "%s"', cookie.name)
    );
    err.original = chrome.runtime.lastError;
    debug(err);
    fn(err);
  });
}

/**
 * Noop.
 */

function noop(){};
