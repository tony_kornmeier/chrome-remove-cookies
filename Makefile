
BIN := node_modules/.bin
DUOFLAGS ?= --development
JADEFLAGS ?= --pretty
ENTRY_SCRIPTS := $(addprefix build/, $(wildcard *.js))
SCRIPTS := $(wildcard *.js lib/*/*.js lib/*/*.jade)
ENTRY_STYLES = $(addprefix build/, $(wildcard *.styl))
PAGES = $(addprefix build/, $(wildcard *.jade))
STYLES := $(wildcard lib/*/*.styl)
ALL := $(ENTRY_SCRIPTS) \
       $(ENTRY_STYLES:.styl=.css) \
       $(PAGES:.jade=.html) \
       build/manifest.json \
       build/icon.png

all: $(ALL)

build/%.js: %.js $(SCRIPTS) build node_modules
	$(BIN)/duo --type js --use duo-jade $(DUOFLAGS) < $< > $@

build/%.css: %.styl $(STYLES) build node_modules
	$(BIN)/duo --type css --use duo-stylus $(DUOFLAGS) < $< > $@

build/%.html: %.jade build node_modules
	$(BIN)/jade $(JADEFLAGS) < $< > $@

build/%: % build
	cp -f $< $@

build:; mkdir -p $@

node_modules: package.json
	npm install
	@touch $@

component.json: components/duo.json
	$(BIN)/duo-pin

clean:
	rm -rf build components

.PHONY: clean all
